package fr.minecraft.MyLogs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class Config
{
	private static String DIR_PATH="./plugins/MyLogs/",FILE_PATH="config.json";
	public String database="", login="", password="";
	public int numberOfLog=5;
	public List<String> worlds=new ArrayList<String>();
	public boolean enableLogs=false;
	
	private static Config instance;
	
	public static Config GetConfig()
	{
		try {
			if(Config.instance==null)
			{
				BufferedReader br=new BufferedReader(new FileReader(DIR_PATH+FILE_PATH));
				String line, content = "";
				while((line = br.readLine())!=null)
				{
					content += line;
				}
				br.close();
				Gson g = new Gson();
				Config.instance = g.fromJson(content, Config.class);
			}
		} catch(FileNotFoundException e2) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur fichier "+DIR_PATH+FILE_PATH+" not found!");
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e2.getMessage());
			Config.CreateFile();
		} catch (IOException e3) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur IOException");
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e3.getMessage());
		} catch (JsonSyntaxException jsonEx) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur JsonSyntaxException");
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+jsonEx.getMessage());
		}
		return Config.instance;
	}

	private static void CreateFile() {
		try {
			File directory = new File(DIR_PATH);
			if(directory.mkdirs())
				Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD+"Directories created");
			File file = new File(DIR_PATH+FILE_PATH);
			if (file.exists() || file.createNewFile()) {
				Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD+"File created: " + DIR_PATH+FILE_PATH+", you need to change the config!");
				Gson g = new Gson();
				Config.instance = new Config();
				String json = g.toJson(Config.instance, Config.class);

				BufferedWriter writer = new BufferedWriter(new FileWriter(DIR_PATH+FILE_PATH));
				writer.write(json);
				writer.close();
			} else {
				Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"File already exists.");
			}
		} catch (IOException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"An error occurred.");
		}
	}

	public String toString()
	{
		return "database: "+this.database+", login: "+this.login+", password: "+this.password;
	}
}
