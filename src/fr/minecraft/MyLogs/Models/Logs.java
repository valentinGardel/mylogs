package fr.minecraft.MyLogs.Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import fr.minecraft.MyLogs.DBConnection;
import fr.minecraft.MyLogs.Config;

public class Logs
{
	private final static String TABLE_LOGS="MyLogs";

	public int id;
	public String date, action;
	public OfflinePlayer player;
	public Location location;
	public Material material;

	private Logs(OfflinePlayer offlinePlayer, Material material, String date, Location location, String action)
	{
		this.player=offlinePlayer;
		this.material=material;
		this.location=location;
		this.date=date;
		this.action=action;
	}

	public static void createTable() throws SQLException
	{
		Statement stmt = DBConnection.getInstance().createStatement();
		stmt.executeUpdate("create table if not exists "+TABLE_LOGS+"(id int AUTO_INCREMENT, x int, y int, z int, worldUUID varchar(50), action varchar(20),material varchar(50), playerUUID varchar(50), date TIMESTAMP, primary key(id, x, y, z, worldUUID))");
		stmt.close();
	}

	@SuppressWarnings("deprecation")
	public static ArrayList<Logs> getLogs(Location l)
	{
		ArrayList<Logs> logs=new ArrayList<Logs>();
		try {
			PreparedStatement st;

			st = DBConnection.getInstance().prepareStatement("Select playerUUID, material, date, action from "+TABLE_LOGS+" where x=? and y=? and z=? and worldUUID=? ORDER BY date DESC LIMIT ?;");

			st.setInt(1, l.getBlockX());
			st.setInt(2, l.getBlockY());
			st.setInt(3, l.getBlockZ());
			st.setString(4, l.getWorld().getUID().toString());
			st.setInt(5, Config.GetConfig().numberOfLog);
			ResultSet res = st.executeQuery();
			while(res.next())
				logs.add(new Logs(
						Bukkit.getOfflinePlayer(UUID.fromString(res.getString("playerUUID"))),
						Material.getMaterial(res.getString("material")),
						res.getTimestamp("date").toLocaleString(),
						null,
						//new Location(Bukkit.getWorld(UUID.fromString(res.getString("worldUUID"))), res.getInt("x"), res.getInt("y"), res.getInt("z")),
						res.getString("action")
						));
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		catch (Exception e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		return logs;
	}

	public static void addLogs(Location l, Player p, String action, String m)
	{
		//insertion
		PreparedStatement st;
		try {
			st = DBConnection.getInstance().prepareStatement("insert into "+TABLE_LOGS+" (x, y, z, worldUUID, action, material, playerUUID, date) values(?, ?, ?, ?, ?, ?, ?, ?);");

			st.setInt(1, l.getBlockX());
			st.setInt(2, l.getBlockY());
			st.setInt(3, l.getBlockZ());
			st.setString(4, l.getWorld().getUID().toString());
			st.setString(5, action);
			st.setString(6, m);
			st.setString(7, p.getUniqueId().toString());
			/*Timestamp date = java.sql.Timestamp.valueOf(java.time.LocalDateTime.now());
			if(p.getName().equalsIgnoreCase("gragonstar"))
				Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD+date.toLocaleString());
				*/
			st.setTimestamp(8, java.sql.Timestamp.valueOf(java.time.LocalDateTime.now()));
			st.executeUpdate();
			st.close();
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		catch (Exception e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
	}

	public String toString() {
		return "Le joueur "+this.player.getName()+ " a "+action+" "+this.material+" le "+this.date;
	}
}
