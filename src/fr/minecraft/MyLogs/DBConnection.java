package fr.minecraft.MyLogs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.google.gson.JsonSyntaxException;

public class DBConnection
{
	private static Connection con=null;

	private DBConnection()
	{
		Config config;
		try {
			config = Config.GetConfig();
			Class.forName("com.mysql.jdbc.Driver");
			DBConnection.con=DriverManager.getConnection(config.database + "?autoReconnect=true",config.login, config.password);
		} catch(SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur lors de la connection");
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
			return;
		} catch (ClassNotFoundException e1) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur class.forname()");
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e1.getMessage());
		} catch (JsonSyntaxException jsonEx) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur IOException");
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+jsonEx.getMessage());
		} catch (Exception e4) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur Exception");
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e4.getMessage());
		}
	}

	public static Connection getInstance()
	{
		try {
			if(DBConnection.con == null || DBConnection.con.isClosed() || !DBConnection.con.isValid(0))
				new DBConnection();
		} catch (SQLException e) {
			new DBConnection();
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+e.getMessage());
		}
		return DBConnection.con;		
	} 
}
