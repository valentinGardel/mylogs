package fr.minecraft.MyLogs;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minecraft.MyLogs.Commands.*;
import fr.minecraft.MyLogs.Listeners.*;
import fr.minecraft.MyLogs.Models.Logs;

public class Main extends JavaPlugin
{

	public void onEnable()
	{
		try {
			Logs.createTable();
			this.getCommand("logs").setExecutor(new LogsCommand());
			this.getCommand("/logsSelector").setExecutor(new LogSelectorCommand());
			
			if(Config.GetConfig().enableLogs)
				getServer().getPluginManager().registerEvents(new LogsListener(), this);
			
			getServer().getPluginManager().registerEvents(new LogsSelectorListener(), this);

			Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN+"Activation MyLogs.");
		} catch (SQLException e) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.RED+"Erreur lors de la creation de la table logs");
			e.printStackTrace();
		}
	}

	public void onDisable()
	{
		Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN+"Désactivation de MyLogs");
	}
}