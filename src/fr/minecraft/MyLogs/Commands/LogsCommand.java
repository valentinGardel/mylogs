package fr.minecraft.MyLogs.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minecraft.MyLogs.Models.Logs;

public class LogsCommand implements CommandExecutor
{
	private static final int DISTANCE_MAX=5;

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		Location location=null;
		if(args.length==0 && sender instanceof Player)
		{
			Player player = (Player)sender;
			location= player.getTargetBlockExact(DISTANCE_MAX).getLocation();
		}
		else if(args.length==4)
		{
			location = new Location(Bukkit.getWorld(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]));
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Commande invalide");
			return true;
		}
		if(location!=null)
		{
			this.displayLogs(sender, location);
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Position invalide");
		}
		return true;
	}

	private void displayLogs(CommandSender sender, Location location)
	{
		ArrayList<Logs> logs = Logs.getLogs(location);
		if(logs.isEmpty())
		{
			sender.sendMessage(ChatColor.GOLD+"Aucun log!");
		}
		else
		{
			sender.sendMessage(ChatColor.GOLD+""+logs.size()+" log(s) pour ce bloc:");
			for(Logs log : logs)
				sender.sendMessage(" "+log.toString());
		}
	}
}
