package fr.minecraft.MyLogs.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LogSelectorCommand implements CommandExecutor
{
	public static final String WAND_NAME=ChatColor.GOLD+"LogSelector";
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			ItemStack item = new ItemStack(Material.STICK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(WAND_NAME);
			meta.addEnchant(Enchantment.VANISHING_CURSE, 69, true);
			item.setItemMeta(meta);
			player.getInventory().addItem(item);
		}
		else
			sender.sendMessage(ChatColor.RED+"Impossible pour un non joueur!");
		return true;
	}

}
