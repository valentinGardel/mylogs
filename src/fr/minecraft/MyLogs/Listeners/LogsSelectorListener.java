package fr.minecraft.MyLogs.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.minecraft.MyLogs.Commands.LogSelectorCommand;

public class LogsSelectorListener implements Listener
{
	@EventHandler
	public void guestCantInteract(PlayerInteractEvent event)
	{
		if(event.getItem()!=null && event.getItem().getItemMeta() != null && event.getItem().getItemMeta().getDisplayName().equals(LogSelectorCommand.WAND_NAME))
		{
			event.setCancelled(true);
			if(event.getAction()==Action.RIGHT_CLICK_BLOCK)
			{
				event.getPlayer().performCommand("logs");
			}
		}
	}
}
