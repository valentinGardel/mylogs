package fr.minecraft.MyLogs.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;

import fr.minecraft.MyLogs.Config;
import fr.minecraft.MyLogs.Models.Logs;

public class LogsListener implements Listener
{
	@EventHandler
	public void onPlayerBucketEmptyEvent(PlayerBucketEmptyEvent event)
	{
		if(!event.isCancelled() && Config.GetConfig().worlds.contains(event.getBlock().getLocation().getWorld().getName()))
		{
			Logs.addLogs(event.getBlock().getLocation(), event.getPlayer(), "vid�", event.getBucket().name());
		}
	}

	@EventHandler
	public void onPlayerBucketFillEvent(PlayerBucketFillEvent event)
	{
		if(!event.isCancelled() && Config.GetConfig().worlds.contains(event.getBlock().getLocation().getWorld().getName()))
		{
			Logs.addLogs(event.getBlock().getLocation(), event.getPlayer(), "rempli", event.getBucket().name());
		}

	}

	@EventHandler
	public void onBlockPlaceEvent(BlockPlaceEvent event)
	{
		if(!event.isCancelled() && Config.GetConfig().worlds.contains(event.getBlock().getLocation().getWorld().getName()))
			Logs.addLogs(event.getBlock().getLocation(), event.getPlayer(), "plac�", event.getBlock().getType().name());

	}

	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent event)
	{
		if(!event.isCancelled()&& Config.GetConfig().worlds.contains(event.getBlock().getLocation().getWorld().getName()))
			Logs.addLogs(event.getBlock().getLocation(), event.getPlayer(), "cass�", event.getBlock().getType().name());

	}
}
